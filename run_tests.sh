#!/usr/bin/env bash

test_type=$1

echo "========================================="
echo "Starting tests..."
echo "========================================="
echo "PYTHONPATH=$PYTHONPATH"
echo "========================================="
coverage run -m unittest discover -s "tests/src/$test_type/" -t . -v
exit_code=$? # save exit code (tests successful or not) to return it at the end of the script

echo ""
echo "========================================="
echo "COVERAGE Report"
echo "========================================="
coverage report -m

mv .coverage .coverage."$test_type"

exit $exit_code
