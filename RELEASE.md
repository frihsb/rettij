# Release a new rettij Version

Releases are automatically handled by the gitlab CI. A new release is triggered when a new tag is created. Only maintainers can create new releases.

1. Go to https://gitlab.com/frihsb/rettij/-/releases/new
1. Fill in the tag information
   1. `Tag name`: Create a new tag by writing the new rettij version number. rettij uses _semantic versioning_ (https://semver.org/).
   1. `Select source`: `master`
   1. `Message`: Depending on major, minor or bugfix version upgrade.
      1. Major: `Breaking changes, new features and bugfixes`
      1. Minor: `New features and bugfixes`
      1. Bugfix: `Bugfixes`
1. `Release title`: Leave this blank (so GitLab will automatically use the tag name).
1. `Release date`: Use the current date.
1. `Release notes`: A short and concise overview of new or changed features and documentations that are relevant to the user. See the template below.
1. Create the release. Once the associated pipeline has finished successfully, the release package will be available at https://gitlab.com/frihsb/rettij/-/packages and https://pypi.org/project/rettij/.

## Release Note Template

```markdown
**Features**

- new_or_changed_feature_1
- new_or_changed_feature_2

**Docs**

- new_or_changed_documentation_1
- new_or_changed_documentation_2

**Bugfixes**

- fix_1
- fix_2
```
