from typing import Any

from rettij.topology.hooks.abstract_connect_hook import AbstractConnectHook
from rettij.topology.network_components.node import Node


class ConnectHook(AbstractConnectHook):
    """
    This class defines a 'connect' hook for testing purposes.
    """

    def execute(self, source_node: Node, target_node: Node, **kwargs: Any) -> None:
        """
        Execute the 'connect' hook for testing purposes.
        """
        print(f"Connect hook of node {source_node.name}.")
