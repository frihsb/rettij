####################################################################################
# From https://docs.bisdn.de/network_configuration/vlan_bridging.html              #
# And https://developers.redhat.com/blog/2017/09/14/vlan-filter-support-on-bridge/ #
####################################################################################

# Enable VLAN filtering
ip link set vxlan-bridge type bridge vlan_filtering 1

# Add port vlan
bridge vlan add vid 10 dev i1 pvid untagged master
bridge vlan add vid 20 dev i2 pvid untagged master

# Add vlan trunk
bridge vlan add vid 10 dev i0 master
bridge vlan add vid 20 dev i0 master