# --- Packages needed to run rettij --- #
-r requirements.prod.txt

# --- Packages needed to develop rettij --- #

# Test coverage
coverage~=7.2

# License checking
liccheck~=0.9.1

# Code documentation
Sphinx~=7.1.1
myst-parser~=2.0.0
furo~=2023.7.26

# Git pre-commit hook configuration
pre-commit~=3.3.3

# Packaging
setuptools~=68.1.2
wheel~=0.41.0

# Type stubs
types-setuptools~=68.1.0
types-PyYAML~=6.0.12
types-six~=1.16.1

# ----------------------------------------------------------------------------#
# The following packages are used and automatically installed by pre-commit
# You do not need to install them via requirements.
# To use the environment set up by pre-commit go to ~/.cache/pre-commit
# and search for and activate the virtual environment created by pre-commit
# ----------------------------------------------------------------------------#

# Code linting
# flake8

# Autoformatting
# black

# Type checking
# mypy
