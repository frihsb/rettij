rettij Network Simulator
======

## What is rettij?

The network simulator rettij is developed by the *[Forschungsgruppe Rechnernetze und Informationssicherheit](https://www.fri.hs-bremen.de/)* at the [Bremen University of Applied Sciences](https://hs-bremen.de/en).
After testing and evaluating various frameworks, we came to the conclusion that the existing network simulators do a great job at what they aim to do, but we never felt like they were the right tool for our purposes.
Therefore, we finally decided to implement our own network simulation framework.

### Use cases

#### Standalone (manual usage)
* Prototyping and pen-testing Docker images
* Learning environment

#### Embedded (usage inside other applications)
* Scripted controls
* Co-simulation

### Features

#### Native co-simulation capabilities
* Can be used in event-based co-simulation environment
* APIs are based on co-simulation requirements

#### Realistic network behavior
* Uses the actual unix network stack, allowing simulation of realistic cyber-attack scenarios
* Isolates simulation traffic via VXLAN tunneling, meaning no noise from Docker, Kubernetes in packet captures

#### Use real and proprietary software
* Usage of containers and VMs allows running real-world applications, including proprietary software you could replicate fully and legally
* Can be used to verify real-world behavior

#### No in-depth programming knowledge needed
* Easy-to-understand-and-write description language for network topologies
* Standalone mode requiring no further programmatic interaction
* Self-explanatory APIs for easy integration (i.e. for co-simulation scenarios) as well as full code documentation [here](https://frihsb.gitlab.io/rettij).

#### Scalability
* Efficient emulation due to the use of Docker containers (as opposed to VMs)
* Can be used in a Kubernetes cluster to have access to a large pool of computing resources

## Tutorial

The tutorial consists of multiple chapters introducing you to the rettij usage and development step-by-step.
Even if you want to develop for rettij, you should always start with the first chapter as it covers some basics like the setup process.

### Getting Started
Here you will learn how to set up a simple simulation environment with a basic initial configuration.

[-> GO TO "Getting Started"](docs/getting-started.md)

### Working with rettij
You need to understand the basics of rettij to build your own network-topologies, create custom components and define simulation sequences.

[-> GO TO "Working with rettij"](docs/working-with-rettij.md)

### Troubleshooting
Some suggestions for fixing commonly encountered roadblocks when working with rettij, Python as well as Kubernetes.

[-> GO TO "Troubleshooting"](docs/troubleshooting.md)

## Contributing to rettij

Please refer to the [CONTRIBUTING](CONTRIBUTING.md) file for information on how to contribute to rettij.

## Contributors

Initial development was done by primarily the following members of the *[Forschungsgruppe Rechnernetze und Informationssicherheit](https://fri.hs-bremen.de)* at the [Bremen University of Applied Sciences](https://www.hs-bremen.de):
* Fabian Niehaus
* Torben Woltjen
* Giacomo Gritzan
* Philipp Kathmann
* Bastian Fraune
