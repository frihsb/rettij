#!/usr/bin/env bash

# Set up the rettij environment and install rettij.
# You can specify a path to a local dist file or specify a specific rettij version to be installed.
# usage: install-rettij.sh [-p <package>] [-v <venv_path>]

while getopts ":p:v:" opt; do
  case $opt in
    p) package="$OPTARG"
    ;;
    v) venv_path="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done


set -x  # print executed commands
set -e  # fail script once any command fails

# Create venv and install rettij
venv_path="${venv_path:-./venv}"
python -m venv "$venv_path"
"$venv_path/bin/pip3" install wheel
"$venv_path/bin/pip3" install "${package:-rettij}"  # https://stackoverflow.com/a/9333006 -> Use 'rettij' if no package was provided

echo "Installed rettij to $venv_path (working directory: $PWD)"
