# Troubleshooting

Info: In every one of these steps change "</full/path/to/rettij/>" to your actual rettij directory before executing.

## Setting/fixing the Python path
If Python modules are not recognized when running rettij, chances are that your PYTHONPATH environment variable is set incorrectly or not at all. You can fix it by adding your rettij directory to PYTHONPATH.  
**Note: This is only relevant when developing rettij. When installing rettij via pip, you do not need to set the PYTHONPATH.**
```
# bash
nano ~/.profile
## Add the following line to the end:
export PYTHONPATH="${PYTHONPATH}:"</full/path/to/rettij/>""
```
On Windows you need to set this variable manually. Go to:
Control Panel > System and Security > System > Advanced System Settings > Environment Variables  
Systemsteuerung > System und Sicherheit > System > Erweiterte Systemeinstellungen > Umgebungsvariablen

You can now create a new System variable called `PYTHONPATH`, if it doesn't exist yet, and add the following directories: `C:\Python27\Lib;C:\Python27\DLLs;C:\Python27\Lib\lib-tk;C:<\full\path\to\rettij>`

## Setting/fixing the PATH environment variable
If your bash shell or command line doesn't recognize executable commands (e.g. `python `, `python3`, `kubectl`, etc.) with the error message `command <command> not found`, you need to add the path to that executable's directory to your PATH variable like this:  

Unix-Systems:
```
# bash
sudo export PYTHONPATH=/usr/local/bin/python3.8
```
Windows:
```
# cmd/powershell
path %path%;<your/python/directory>
## To find out <your/python/directory> on windows, use:
where python3
```
Alternatively you can manually edit your `Path` System variable manually on Windows under  
Control Panel > System and Security > System > Advanced System Settings > `Environment variables`.

## (Re-)Creating the virtual environment
If your `venv` directory is gone for some reason (re-cloning the project for example), follow these steps:

### Using bash/command line

Unix-Systems:
```
# bash
python3 -m venv </full/path/to/rettij>/venv
```
Windows:
```
# cmd/powershell with correctly set PATH
python -m venv <\full\path\to\rettij>\venv
# cmd/powershell without PATH
<your/python/directory> -m venv <\full\path\to\rettij>\venv
```

### Using Pycharm
File > Settings > Project: rettij > Python Interpreter > cog wheel(upper right corner) > Add... > Pick Python3.8 from the drop-down > OK > Apply

If your `venv` directory is still present, and you just want to access it, use:  

Unix-Systems:
```
# bash
source <\full\path\to\rettij>\venv\bin\activate
```
Windows:
```
# cmd / powershell
<\full\path\to\rettij>\venv\Scripts\activate
# PyCharm Terminal in Windows
"cmd.exe" /k ""%CD%\venv\Scripts\activate""
```

## Deleting Pods, Containers and Images
Sometimes a container or image gets corrupted and won't function correctly. Maybe you have run out of space on your hard drive because of large images or long-running containers. Try cleaning up your simulation environment. Kubernetes features the following useful commands.

### How to view Kubernetes Nodes, Namespaces, Pods etc.
List containers, nodes, namespaces, pods, images and yml-files:
```
# view nodes
kubectl get nodes -o wide
# view namespaces
kubectl get namespaces -o wide
# view pods
kubectl get pods --all-namespaces
# view all pods in a specific namespace
kubectl get -n <namespace_name> pods
# view a pod's yaml
kubectl get -n <namespace_name> <pod_name> -o yaml
# view running containers
k3s crictl ps
# view images
k3s crictl images
```  
### How to delete Kubernetes Pods, Namespaces and images
Remove one or more containers, pods, or images:
```
# remove a pod
kubectl delete -n <namespace_name> pod <pod_name>
# remove a namespace
kubectl delete ns <namespace_name>
# remove one or more images
k3s crictl rmi <image id>
```

Delete all namespaces and their contents related to rettij:
```
# bash
sudo kubectl delete namespace $(sudo kubectl get namespace -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}" | grep 'rettij' )

# Powershell
kubectl delete namespace $(kubectl get namespace -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}" | Select-String -Pattern 'rettij')
```
You can find more useful `k3s crictl` commands [here](https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md#usage).
You can find more useful `kubectl` commands [here](https://kubernetes.io/docs/reference/kubectl/cheatsheet/).

P.S.: If your system does not recognize the `kubectl` command, you need to add the path to your Kubernetes directory to your PATH variable like in [section 1](#settingfixing-the-path-environment-variable).  

### How to view Docker containers and images
Docker is neither a requirement nor the default setting for rettij.

```
# list all containers
docker container ls --all
# list all images
docker image ls --all
```

### How to delete Docker containers and images
Docker is neither a requirement nor the default setting for rettij.

```
# remove all unused containers
docker container prune --all
# remove all unused images
docker image prune --all
```
You can find more on these docker commands [here](https://docs.docker.com/engine/reference/commandline/docker/).  
You can find more on keeping your docker registry clean [here](https://medium.com/better-programming/cleanup-your-docker-registry-ef0527673e3a) and [here](working-with-rettij.md#useful-kubernetes-commands).

## Restarting
### Restarting a Kubernetes pod
Kubernetes Pods are meant to be ephemeral, meaning short-lived and disposable.
As such, it is not possible to restart a Pod using `kubectl`.

### Restarting Kubernetes / k3s
Unix-Systems:
```
sudo systemctl restart k3s
# or
sudo service k3s restart
```

### Restarting a Docker container
Docker is neither a requirement nor the default setting for rettij.

```
docker restart <container_name>
```

### Restarting the Docker daemon
Docker is neither a requirement nor the default setting for rettij.

Unix-Systems:
```
sudo systemctl restart docker
# or
sudo service docker restart
```
Windows:
Right click on Docker-for-Windows icon on the Task bar > Restart...

### Restarting the host system
1. Exit the simulation
2. restart the PC that rettij/k3s is running on.

## Re-installing
### Uninstall Kubernetes / k3s
```
# Stop running server to remove all resources
/usr/local/bin/k3s-killall.sh
# uninstall k3s
/usr/local/bin/k3s-uninstall.sh
```
### Install Kubernetes / k3s
Please refer to [Getting Started](getting-started.md#2-install-and-configure-k3s) on how to install `k3s`.

### Uninstall Docker
Docker is neither a requirement nor the default setting for rettij.

```
# uninstall docker
## identify what package you have installed
dpkg -l | grep -i docker
## uninstall
sudo apt-get purge -y docker-engine docker docker.io docker-ce docker-ce-cli
sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce
# delete all images, containers, volumes, and user created configuration files on your host
sudo rm -rf /var/lib/docker /etc/docker
sudo rm /etc/apparmor.d/docker
sudo groupdel docker
sudo rm -rf /var/run/docker.sock
```

On Windows simply uninstall and reinstall Docker Desktop manually under  
`control panel > uninstall programm > right-click on Docker Desktop > uninstall`  
`Systemsteuerung > Programm deinstallieren > Rechtsklick auf Docker Desktop > deinstallieren`

### Install Docker
Docker is neither a requirement nor the default setting for rettij.

```
# Download the convenience script for installing Docker
curl -fsSL https://get.docker.com -o get-docker.sh
# Run the installation script
sudo sh get-docker.sh
# Give your current user privileges to run the Docker daemon using a Unix group
sudo usermod -aG docker $USER
```

On Windows you can download Docker Desktop [here](https://docs.docker.com/docker-for-windows/install/).
Or using [chocolatey](https://chocolatey.org/):
```
choco install docker-desktop
```

## Common error messages and their solutions
```
ImageInspectError
```
[Prune all Images](#deleting-pods-containers-and-images) and restart the simulation.

---

```
ErrImagePull
```

The container image can not be loaded. This usually means that the image name/tag is incorrect or that DockerHub is not available.

---

```
host unreachable` (containers/nodes can't talk to each other)
```

If your Sim setup is correct, this is a problem with docker (most commonly encountered on Windows) and is most quickly solved by [reinstalling](#uninstall-docker).

---

```
Command 'python3' / 'pip' / 'docker' / 'kubectl' not found`
```

[The PATH environment variable isn't set correctly](#settingfixing-the-path-environment-variable)

---

```
ModuleNotFoundError: No module named 'rettij.common'; 'rettij' is not a package
```

[Activate the virtual environment](#re-creating-the-virtual-environment)

---

```
ModuleNotFoundError: No module named 'kubernetes‘
```

Install requirements from `./requirements.txt` using `pip install -r requirements.txt`

---

```
rettij.exceptions.host_not_available_exception.HostNotAvailableException: Connection to host https://my-kubernetes:6443 failed. Reason: 'HTTPSConnectionPool(host='my-kubernetes', port=6443): Max retries exceeded with url: /api/v1/ (Caused by SSLError(SSLCertVerificationError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: certificate has expired (_ssl.c:1131)')))'
```

Rotate the k3s certificates with `sudo k3s certificate rotate`. Afterwards, restart the k3s server, usually with `sudo systemctl restart k3s`.

---

This list is incomplete by nature. You can help by expanding it.
