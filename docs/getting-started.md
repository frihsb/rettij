# Getting Started

## Definitions
The following terms are used in rettij with these definitions:
* `Topology`: Defines the simulation network
	* `Node`: Network component (Switch, Router, Container/PC/Server)
	* `Interface`: Network interface (NIC) of a Node
	* `Channel`: Logical connection between two interfaces (like an ethernet cable)
* `Command`: Instructions to be run within a Node during simulation
* `Sequence`: Defines automatically executed command sequence
	* `Scheduled Sequence`: Commands are executed at a specific time in the simulation
	* `Script Sequence`: Commands are run immediately and in order of declaration

## Requirements
Make sure to set up the following requirements. Step-by-step instructions on how to do this are provided [below](#step-by-step-guide-for-installing-rettij-on-linux-using-k3s).
* Python 3.8 with pip
* A Kubernetes cluster, with the following privileges / properties:
	* Privileges to create and delete namespaces
	* Privileges to run pods in privileged mode

## Step-by-step-guide for installing rettij on Linux using k3s
This step-by-step guide contains instructions for installing rettij on Linux (Ubuntu), using k3s for the Kubernetes cluster.
These steps can be run automatically using the `./install-rettij.sh` script.
All you need to do is to activate the venv after the script has finished running.

### 1. Install Python 3.8 (Most versions of Ubuntu 18.04 or newer come with Python pre-installed):
````bash
# Update your apt package lists
sudo apt-get update
# The software-properties-common package allows your packet manager to add PPA repositories
sudo apt-get install software-properties-common
# Deadsnakes is a PPA with newer and older releases than the default Ubuntu repositories
sudo add-apt-repository ppa:deadsnakes/ppa
# Update your apt package lists again
sudo apt-get update
# Rettij's currently uses Python 3.8. Install Python 3.8 using:
sudo apt-get install python3.8 python3.8-venv
````

### 2. Install and configure k3s

````bash
# Create the k3s directory
sudo mkdir -p /etc/rancher/k3s

# Create the k3s configuration file
echo '
write-kubeconfig: /root/.kube/config
write-kubeconfig-mode: "0644"
disable:
  - "traefik"
  - "servicelb"
  - "metrics-server"
' | sudo tee /etc/rancher/k3s/config.yaml

# Install k3s and start cluster
sudo curl -sfL https://get.k3s.io | sh -s -

# Ensure the initial kubeconfig was written properly
# Output should look like this:
# -rw-r--r-- 1 root root 2957 Mai 10 15:20 /root/.kube/config
sudo ls -l /root/.kube/config

# If the prior step fails, restart k3s and repeat the prior step
sudo systemctl restart k3s

# Create the ~/.kube/ directory and link kubeconfig to user home, so rettij and kubectl can be used both with and without sudo
mkdir -p ~/.kube && sudo ln /root/.kube/config ~/.kube/config

# Check server nodes
sudo k3s kubectl get nodes
````

### 3. Instantiate a virtual environment and activate it:
````bash
# Instantiation
python3.8 -m venv ./venv
# Activation
source venv/bin/activate
````

### 4. Once inside the venv, install rettij via pip3:
Please make sure that you are inside the venv - you will see a (venv) prefixed to your prompt in the terminal:
```bash
(venv) ~$
```

Install wheel and rettij via pip3. `Wheel` is required for building some dependencies:
````bash
pip3 install wheel
pip3 install rettij
````

### 5. Test your installation with the rettij command
````bash
rettij -h
````

You can run the example topology and sequence by copying their paths from the help output.
If everything was set up correctly, rettij should now run the example simulation.  
As you can see in the output log, our example sends a ping command from simulation node `client0` to `10.1.1.2` (IP of `client1`) and from simulation node `client1` to `10.1.1.1` (IP of `client0`).

### 6. Learn how to use rettij

Refer to the ["Working with rettij" documentation](working-with-rettij.md) to learn more about how to use rettij.

Gather hands-on experience by following our [example scenario](example.md).

## Troubleshooting

<details>
<summary>Click here for troubleshooting suggestions</summary>

- Regarding step 2: For trouble with the kubeconfig, please refer to the [Tell rettij how to access the Kubernetes cluster](working-with-rettij.md#tell-rettij-how-to-access-the-kubernetes-cluster) chapter.
- Regarding step 3: If you have trouble setting up a Virtual Environment using the command line, please refer to the [official documentation](https://docs.python.org/3.8/library/venv.html).
Make sure to also activate the environment!  
- Regarding step 4: If the `pip3` command doesn't work, please refer to the [official documentation](https://docs.python.org/3/installing/index.html#pip-not-installed)

</details>

Please also refer to the [troubleshooting document](troubleshooting.md).
