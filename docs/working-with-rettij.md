# Working with rettij

To use rettij for network simulations, we need the typical ingredients of a network. For rettij this are
* A network-topology containing:
  * Infrastructure devices such as routers, switches or just clients and servers. These are referred to as "(simulation) nodes".
  * Channels ("virtual network-cables") to connect these devices.
* Sequences defining the execution order and timing of commands in the simulation.

## Command line parameters

The following command line parameters can be used with rettij:

```text
usage: rettij [-h] [-t TOPOLOGY] [-s SEQUENCE] [-k KUBECONFIG]
              [--components COMPONENTS]
              [-l [{CRITICAL,ERROR,WARNING,INFO,DEBUG}]]
              [-c [{CRITICAL,ERROR,WARNING,INFO,DEBUG,NOTSET}]]
              [--monitoring] [--monitoring-config MONITORING_CONFIG] [-q]

rettij standalone script

optional arguments:
  -h, --help            show this help message and exit
  -t TOPOLOGY, --topology TOPOLOGY
                        Path to topology file (required).
                        Use '<rettij_install_path>/rettij/examples/topologies/simple-switch_topology.yml'
                        to apply an example topology.
  -s SEQUENCE, --sequence SEQUENCE
                        Path to simulation sequence file (optional).
                        Use '<rettij_install_path>/rettij/examples/sequences/basic_ping_sequence.py'
                        to run an example sequence.
  -k KUBECONFIG, --kubeconfig KUBECONFIG
                        Path to kubeconfig file (optional).
  --components COMPONENTS
                        Path to the custom components root directory
                        (optional).
  -l [{CRITICAL,ERROR,WARNING,INFO,DEBUG}], --loglevel [{CRITICAL,ERROR,WARNING,INFO,DEBUG}]
                        Set loglevel for logfile, default: INFO
  -c [{CRITICAL,ERROR,WARNING,INFO,DEBUG,NOTSET}], --console [{CRITICAL,ERROR,WARNING,INFO,DEBUG,NOTSET}]
                        Set loglevel for console, default: INFO
  --monitoring          Activate monitoring logging
  --monitoring-config MONITORING_CONFIG
                        Path to the monitoring logging config file.
  -q, --no-cli          Disable the CLI (quiet / non-interactive mode).
                        Default: CLI enabled.
```

## (Network) Topology
The topology file defines the network topology for the simulation:
* When using the `main.py` script, the path to a topology file must be passed with `-t <path-to-file>` or `--topology <path-to-file>`.
* Example topologies can be found at `rettij/examples/topologies/`.

<details>
<summary>CLICK HERE to show an example topology</summary>

```yaml
version: '1.1'

nodes:
  - id: client0
    device: container
    component: simple-runner
    interfaces:
      - id: i0
        channel: c0
        ip: 10.1.1.1/24
        mac: 1A:2B:3C:4D:5E:01
        data-rate: 1gbps
    routes:
      - network: 0.0.0.0/0
        gateway: 10.1.1.2
        metric: 50
  - id: switch0
    device: switch
    component: simple-switch
    interfaces:
      - id: i0
        channel: c0
      - id: i1
        channel: c1
      - id: i2
        channel: c2
#      - id: i3    # enable this when adding the host bridge
#        channel: c3
  - id: client1
    device: container
    component: simple-runner
    interfaces:
      - id: i0
        channel: c1
        ip: 10.1.1.2/24
        mac: 1A:2B:3C:4D:5E:02
        data-rate: 1gbps
    routes:
      - network: 0.0.0.0/0
        gateway: 10.1.1.1
        metric: 50
  - id: host0
    device: host
    component: host
    interfaces:
      - id: i0
        channel: c2
        ip: 10.1.1.3/24
        mac: 1A:2B:3C:4D:5E:03
        data-rate: 1gbps
#      - id: i1  # enable and adjust this to add the host bridge
#        channel: c3
#        external:
#          interface: ens38
#          networks:
#           - 192.168.0.0/24
#           - 192.168.1.0/24

channels:
  - id: c0
    data-rate: 100Mbps
    delay: 2ms
```
</details>

### Keys

The topology defines *nodes* and *channels*.
*Nodes* are the hosts in the simulation network.
They have *interfaces* that are connected to interfaces of other nodes via *channels*.

`version`, `nodes` and `channels` are the top-level keys in the topology file.

**Important:** All relative paths in the topology that start with `./` or `../` will automatically be resolved using the directory of the topology file as the base path.

#### version
_Required: No (Will be required in the future)_

The version of the supported topology file format.
The topology versioning uses `x.x` as version numbers, meaning `major.minor`.

Minor version increases are backwards compatible.
Major versions are **not** forwards or backwards compatible and require a matching version of rettij.

#### nodes
_Required: Yes_ \
_Maximum number_: 100 nodes of type `container`, `hub`, `switch` and `router` combined per Kubernetes host ([imposed by Kubernetes](https://github.com/kubernetes/community/blob/master/sig-scalability/configs-and-limits/thresholds.md)), otherwise limited by performance.

This key contains a list of node-definitions.
A "node" describes one "host" in a network.
It is described and deployed in the form of a Kubernetes container specification or deployment file.
That means that a node can consist of multiple containers ("services").
However, be aware that only one of those services can be connected to the simulated network at one time.

Each node has the following key-value pairs:

##### id
_Required: Yes_ \
_Maximum length_: 20 characters (imposed by us)

A unique identifier for each node.
It is used to generate the Kubernetes pod name.

It is required to follow the [DNS label standard](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-label-names) as defined in RFC 1123. This means the id must:
* contain at most 63 characters
* contain only lowercase alphanumeric characters or '-'
* start with an alphanumeric character
* end with an alphanumeric character

##### device
_Required: Yes_

The `device`-field defines the class of the node.
The node device defines how the node lifecycle, networking and command interface will be handled by rettij.
They can be split in two categories: Predefined network components and (external) custom applications.

Possible values are for the predefined network components are:

* `hub` represents an old-school network hub.
* `router` represents an FRR-based network router.
* `switch` represents a network switch.

The options for connecting custom applications are:

* `container` means that this node is represented by a Kubernetes container.
* `vm` means that this node is represented by a VM (virtual machine).
* `host` means that this node is represented by the system running rettij.
Naturally, there can only be one host-node in the topology.
The `service-name` always has to be set to `host`.
Rettij will always try to automatically setup and remove the interfaces as defined in the topology.
If that is not a possibility, it will display the `bash` command used for setup and removal. \
**Known limitations:**
    * Does not work with `k3d` or `Docker for Windows`.
    * Only tested with `k3s` on Ubuntu.
    * Automated setup:
        * Only works on Unix-based systems
        * Requires the `ip` command
        * Only works when rettij is run as root
        * Only works if the host system is the Kubernetes cluster master node

##### component
_Required: Yes_

The `component`-keyword defines which kind of component the `container`-device actually is.

It can be defined in three ways:
* Component template (default, reusable)
* Shorthand in-place definition of a single container
* Full in-place Pod specification

Component templates (such as the components shipped with rettij) are referred to by name.
```yaml
nodes:
  - id: node0
    device: container
    component: simple-runner  # Name of the component directory
```
The following default components are available as of right now:
* simple-runner: A lightweight, debian-based client with pre-installed networking tools.
* simple-switch: A lightweight, debian-based switch
* simple-router: A Debian and FRRouting based router with extensive functionality.
* host: A placeholder for integration of the host system into the simulation.

For more information on the default components, please refer to the respective image resources in the `images/` directory as well as the component specifications in the `rettij/components/` directory.

For instructions on how to add your own components in-place or via component template, please refer to [Custom components for rettij](#custom-components-for-rettij).

##### interfaces
_Required: Yes_ \
_Maximum number_: Tested up to 6000, limited by performance.

The `interfaces` defines the interfaces of the node.
The real world equivalent would be the Ethernet ports of a computer.
Each interface is modelled by a number of properties:

* `id` sets per node unique identifier.
Maximum length is 15 characters ([imposed by `ip`](https://stackoverflow.com/questions/24932172/what-length-can-a-network-interface-name-have)).

* `channel` specifies the channel that is connected to this interface

* `data-rate` (optional) limits the data-rate to a maximum speed (either the channel or the interface limits the speed; whichever is smaller).
  For possible units refer to the [data-rate specification of a channel](#channels). \
**Attention: Data rates lower than 56 kbit/s may lead to unintended delays for ICMP and possibly other basic protocols.**

* `ip` (not used for hubs, switches and host bridge interfaces) specifies the ip address of the interface and implicitly the network to which the interface is connected.
  It must be given in CIDR notation.

* `mac` (optional) sets a MAC address for the interface

* `external` (optional):
    * Define that the host system should act like an ethernet bridge, transparently connecting a physical NIC to the simulation.
    The bridge allows the connection of external devices and networks to the simulation via ethernet.
    * If using this with VMs, ensure that the virtual switch or bridge allows `MAC address spoofing` (Hyper-V) or has `MAC address changes` set to `Accept` (ESXi).
    When using Proxmox, disable the `MAC filter` in the firewall
    * Has the following subkeys:
        * `networks` :
            * Define the external networks connected to the simulation.
            This has to be done for ALL external networks.
            All traffic for networks not explicitly allowed will be blocked.
            * Values (example):
              ```yaml
              - 10.1.0.0/16
              - 192.168.16.0/24
              ```
        * `interface`:
            * Define the physical NIC of the host system that the external networks are connected to.
            * Values (example): `eth1`
            * **Important**: The interface must not have any ip address configured and its `MTU` has to be set to `1400`.
            If the system supports automated setup (see [device type 'host'](#device)), this will be configured automatically (and reset after the simulation).
            Otherwise, you can use the following commands:
               ```bash
              # Remove all ip addresses from the interface (not persistent through reboot!)
              sudo ip address flush dev <iface>

              # Set the interface mtu (possibly not persistent through reboot!)
              sudo ip link set dev <iface> mtu 1400
              ```

##### config
_Required: No_ \

The `config` key is optional and may contain the following optional properties:

* `execution-host` (Device type: `container`, `router`, `switch`, `hub`):
  * Specifies the Kubernetes cluster node on which a specific simulation node should be executed.
  Retrieve all nodes in the Kubernetes cluster with the following command: `kubectl get nodes`.
  * Values (example): `kubernetes-worker-01`
* `config-dir` (Device type: `router`):
  * Specifies the configuration directory for the `simple-router` component.
  Will be resolved with the `rettij/user/` directory as base path.
  * Values (example): `frr/test_router1`

Additionally, any user-defined custom properties can be declared here and later retrieved via `Node.custom_config["<my_property>"]`.

##### routes
_Required: No_ \

The `routes` specify static connections of the node.
Routes can be defined by the following properties:

* `network` defines which target network that route is meant for.
`0.0.0.0/0` is the target network for the default route specified in CIDR notation.
* `gateway` defines the next hop on the route to the target network; it is referred to by an ip address.
In case of the default route, that ip address has to belong to an interface in the same network.
* `metric` (optional) is used by a router's routing algorithm to make weighed decisions on choosing a path through a network.

#### channels
_Required: No_ \
_Maximum number_: No upper limit imposed.

This key contains a list of channel-definitions.
Each channel represents a simulated cable, i.e. "an ethernet cable connected to two nodes".

You only need to define channels explicitly if you want to set an attribute like `data-rate` or `delay`.
Otherwise, you do not need to define the channel here as it will be implicitly created once referenced by an interface.

Each channel has the following key-value pairs:

##### id
_Required: Yes_ \
_Maximum length_: 20 characters (imposed by us)

`id` of the channel.
Typically simple and short, e.g. "c0", "c1" etc.

##### data-rate
_Required: No_ \
_Maximum Value_: 4294967295 bps / 4194303 kbps / 4095 mbps / 3.999 gbps / 0.003 tbps (_TC stores data-rates as a 32-bit unsigned integer in bps internally, so we can specify a max rate of 4294967295 bps._)

Data rate of the simulated channel.
The simulator will limit the data-rate to the data-rate of the connected nodes' interfaces or the data-rate of the channel, depending on which one is lower (the slowest part defines the data-rate of the connection).

The following units can be used with rettij (unit names are not case-sensitive):

* `bit` for bit per second
* `kbit` for kilobit per second
* `mbit` for megabit per second
* `gbit` for gigabit per second


* `kibit` for kibibit per second
* `mibit` for mebibit per second
* `gibit` for gibibit per second


* `bps` for byte per second
* `kbps` for kilobyte per second
* `mbps` for megabyte per second
* `gbps` for gigabyte per second


* `kibps` for kibibyte per second
* `mibps` for mebibyte per second
* `gibps` for gibibyte per second


See [the manpages](http://man7.org/linux/man-pages/man8/tc.8.html) of the `tc`-command for more information.

**Attention: Data rates lower than 56 kbit/s may lead to unintended delays for ICMP and possibly other basic protocols.**

**Attention: Applying a data rate on a channels limits the rate on both ends, thus at least quadrupling the transmission time for messages with a size exceeding the data rate.**

One thing to consider when using tc is how data rate and delay are applied:

1. Split received data into packets based on the data rate
2. The delay defines the propagation delay (and not the transmission delay which would be specified by limiting the data-rate). Please note, that `tc` only applies the delay once per received data, not per outgoing packet.
This means that sending a packet larger than the data rate will only trigger the delay once, not for every split packet in the queue.


##### delay
_Required: No_ \
_Maximum Value_: 2447877ms / 247s (imposed by `tc`)

Delay of the simulated channel. Possible units may be:

* `ms` for milliseconds
* `s` for seconds
* See [the manpages](http://man7.org/linux/man-pages/man8/tc.8.html) of the `tc`-command for a complete list of possible units.

### Validation

A topology is validated twice; the schema-validator makes sure that the topology is formally correct and the address-, channel- and node-validators make sure that it is also logically correct (e.g. that a channel does not connect more or less than two nodes).
The schema-validator validates a topology against a JSON-schema that can be found at `resources/schemas/topology_schema.json`.

For additional information, see `resources/schemas/README.md`

### Custom components for rettij

While rettij ships with a couple of "simple" images (components) for basic functionality, you can, and likely will need to, create your own task- or software-specific images for the simulation.

A rettij component primarily consists of two parts: a *Docker image* and either an in-place definition in the topology or a *component directory containing a template file*.
Please refer to [Topology -> Component](#component) to learn how to define a component in-place, and to [Component template](#component-template) for the template file explanation.

You can also define Python scripts to be run as hooks during different times during the simulation. Refer to section [Hooks](#hooks) for more information.

**When to use the shorthand in-place definition:**

The simplest way to define a component is the shorthand in-place definition.
However, this can only be done for simpler cases where a component only has a Docker image (i.e. no specific settings or hooks).

Features:
* A single container per component
* Default configuration
* Hooks cannot be used
* Okay re-usability (definition has to be copied, but is short)

**When to use the full in-place definition:**

The best way to define a complex components requiring lots of Node-specific settings is the full in-place definition.

Features:
* Full Pod specification with multiple containers, volumes etc.
* Individual configuration per Node
* Hooks cannot be used
* Bad re-usability (definition has to be copied and is usually extensive)

**When to use the template file:**

While you can define a component directly inside the topology, this has two caveats:
1. Multiple (complex) in-place definitions can make the topology unwieldy and hard to read and modify.
2. Hooks are only available when using component templates.  

For complex topologies with Nodes sharing the same component and overall configuration as well as for components that are intended to be heavily reduced, you should therefore use a component template.

Features:
* Full Pod specification with multiple containers, volumes etc.
* Fixed configuration per component
* Allows for usage of hooks
* Great re-usability (template can simply be referenced to by name)

#### Container image
To use a container image in rettij the following criteria have to be met:
* The package `iproute2` is installed inside the image and the `ip` command is available.
* The supported default shell is `bash` (functionality with other shells has not been verified).
  Since Alpine uses `ash` rather than `bash`, alpine-based images are not officially supported.
* The image is available via a registry that can be reached from every node in your Kubernetes cluster.
  If your registry requires authentication to list and download images, you need to [configure your cluster accordingly](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).

#### Shorthand in-place definition
This kind of in-place definition allows easy definition of Nodes with a single container and no additional configuration.

Only the `image` key is obligatory, which configures the container image path in a registry.

Example:
```yaml
nodes:
  - id: node0
    device: container
    component: {image: frihsb/rettij_simple-runner:latest}  # Container image path (required)
```

#### Full in-place definition
This kind of in-place definition allows use of the entire attribute set from the Kubernetes pod container specification, for example enabling volume mounting.

The key `containers` is mandatory and must contain at least one container definition.
Each container definition must at least contain the `image` and the `name` key.

You can define multiple containers for a single component.
To do so, simply add more containers to the `containers` list.
All containers in a Pod share storage and networking namespaces.
From the view of a container, it looks like it is running on the same machine as all other containers in the Pod.


Minimal example:
```yaml
nodes:
  - id: client01
    device: container
    component:
      containers:                                 # Container list (required, must contain at least one container)
        - image: frihsb/rettij_simple-runner:1.0  # Docker image path (required)
          name: simple-runner                     # Container name (required, arbitrary)
```

Example with volume mounting:
```yaml
nodes:
  - id: client01
    device: container
    component:
      containers:                                 # Container list (required, must contain at least one container)
        - image: frihsb/rettij_simple-runner:1.0  # Docker image path (required)
          name: simple-runner                     # Container name (required, arbitrary)
          volumeMounts:                           # -v-v-v-v-v-
            - name: data                          #
              mountPath: /data                    #
      volumes:                                    # Additional configuration
        - name: data                              # (optional)    
          hostPath:                               #
            path: ./data                          #
            type: Directory                       # -^-^-^-^-^-
```

#### Component template
The `component template` is used to define which container image and settings a component uses.
It consists of either a Kubernetes deployment or just the container specification for a Kubernetes pod.

The container specification looks like this:
```yaml
containers:
  - image: frihsb/rettij_simple-runner:1.0  # Docker image path (required)
    name: simple-runner                     # Container name (required, arbitrary)
```

Example with volume mounting:
```yaml
containers:                                 # Container list (required, must contain at least one container)
  - image: frihsb/rettij_simple-runner:1.0  # Docker image path (required)
    name: simple-runner                     # Container name (required, arbitrary)
    volumeMounts:                           # -v-v-v-v-v-
      - name: data                          #
        mountPath: /data                    #
volumes:                                    # Additional configuration
  - name: data                              # (optional)    
    hostPath:                               #
      path: ./data                          #
      type: Directory                       # -^-^-^-^-^-
```

You can also create a Kubernetes deployment file from a docker-compose file using [Kompose](https://kompose.io/).
The resulting file can be used as a template file, and needs to be renamed and placed in the `components` directory as explained below.
If the conversion creates multiple deployments, only the first one will be used.

Example of a Kubernetes deployment:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: simple-runner-deployment
  labels:
    app: simple-runner
spec:
  selector:
    matchLabels:
      app: simple-runner
  template:
    metadata:
      labels:
        app: simple-runner
    spec:
      containers:
      - image: frihsb/rettij_simple-runner:1.0  # Docker image path (required)
        name: simple-runner                     # Container name (required, arbitrary)
```

The component template needs to reside in a folder named like the component and be named like the component itself.
For the component named `my-component`, the directory structure would look like this:
```
<component_dir>/
    my-component/
      my-component.yml
```
You can then refer to that component in your topology by typing `component: my-component`.

While the default components reside inside the `rettij/components` directory, you can supply your own folder path for rettij to look for components in.
This is done via the `--components` argument for `main.py`.

#### Hooks
_Requires: The component directory and template file_

To define hooks, place custom Python scripts with the pre-defined names shown below in the following directory structure:

```
<component_dir>/
    my-component/
        my-component.yml
        hooks/
            pre-deploy.py
            post-deploy.py
            connect.py
            post-connect.py
            pre-teardown.py
```

These scripts must contain the following classes which will be loaded when the topology is parsed.
The order of hooks displayed here is the same as the order in which they will be executed.

| File              	| Execution time                                             | Class                                      	| Import path                                        	|
|-------------------	| --------------------------------------------------------   |--------------------------------------------	|----------------------------------------------------	|
| `pre-deploy.py`   	| Before node(s) are deployed                                | `PreDeployHook(AbstractPreDeployHook)`     	| `rettij.topology.hooks.abstract_pre_deploy_hook`   	|
| `post-deploy.py`  	| After all nodes are deployed                               | `PostDeployHook(AbstractPostDeployHook)`   	| `rettij.topology.hooks.abstract_post_deploy_hook`  	|
| `connect.py`  	    | When `rettij.connect(<src_node>, <dst_node>)` is called    | `ConnectHook(AbstractConnectHook)`   	        | `rettij.topology.hooks.abstract_connect_hook`  	    |
| `post-connect.py`  	| When the `connect` hook has finished                       | `PostConnectHook(AbstractPostConnectHook)`    | `rettij.topology.hooks.abstract_post_connect_hook`  	|
| `pre-teardown.py` 	| Before the node(s) are taken down                          | `PreTeardownHook(AbstractPreTeardownHook)` 	| `rettij.topology.hooks.abstract_pre_teardown_hook` 	|

Your hook logic has to be implemented in the `execute()` method that is defined by the abstract base classes.

##### Example usages
Some example usages for the hooks are listed below:

- `pre-deploy.py`: Create configuration files that are dependent on the specific topology used.
- `post-deploy.py`: Configure and start software that requires network interfaces to be set up, like `suricata` listening on specific interfaces.
- `connect.py`: Connect two Nodes inside rettij, analogous to how `mosaik.world.connect()` connects two entities.
- `post-connect.py`: Start a software that requires the connections to be set up.
- `pre-teardown.py`: Retrieve PCAP files from the Node before it is taken down.

##### Distinct properties of the `connect` hook
Usage of the `connect` hook was conceptualized to be similar to the `mosaik.world.connect()` method.
It is important to know though, that at the time of execution of the `connect` hooks, all Nodes and Channels are already present, so network connectivity is fully established.
The purpose of the hook is to configure attribute-based communication destinations.

The following sequence aims to clarify the process:
1. The user wants Node A to always send the value of attribute "attr_a" to Node B when its value changes.
1. To do so, `rettij.connect(node_a, node_b, {'attr': 'attr_a'})` needs to be called
1. Once rettij has deployed its network, the `connect` hook of Node A will be called.
1. This hook needs to configure the following information within the Node in some user-defined manner:
   "If an update to 'attr_a' is received via the co-simulation interface, send the new value of this attribute to Node B".
   An implementation of the hook might simply place the JSON string `{'attr_a': '10.0.0.2'}` in a `targets.json` file on the Node, where `10.0.0.2` is an IP address of Node B within the simulation.
1. The software running within the Node uses the information transmitted by the `connect` hook to determine how to handle updated attributes values from the co-simulation interface.

In order to facilitate usage in mosaik scenarios, `rettij.connect()` may be called before the simulation network is deployed.
rettij will queue up all connection parameters and set them up once the network is fully deployed, as is described in the table above.

The call sequence for the `connect` hook can be found further down in section [Kubernetes and rettij: Connect Hook](#connect-hook).

## Sequences
Sequences are collections of Commands which are defined before the simulation starts.
There are two types of sequences, the `ScriptSequence` and `ScheduledSequence`.

Commands within _ScriptSequences_ are simply executed in the order they are defined in.
You can copy-paste instructions from the CLI into a ScriptSequence, just like in a Python script.
A ScriptSequence that was passed to rettij as a parameter will be executed immediately once the simulation starts.
If the Sequence was loaded via the CLI, it will be executed at the next call of `rettij.step()`.
This happens automatically when using standalone mode.

_ScheduledSequences_ define a schedule for the simulation.
For each time increment in the simulation (i.e. `1`,`2`,`3` etc.), multiple Commands can be scheduled.
Once `rettij.step(time)` is called with the corresponding time, the corresponding Commands will be executed in the order they were defined in.
This happens automatically when using standalone mode, which starts execution at `time=0`.

The path to a custom sequence file for the `main.py` can be set via the command line argument `-s <path-to-file>` resp. `--sequence <path-to-file>`.

The sequence must **always** contain one of the Sequence classes and implement the `define()` method.

This is the signature for a `ScriptSequence`:
```python
class ScriptSequence(AbstractScriptSequence):

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
```

<details>
<summary>CLICK HERE to show a `ScriptSequence` example</summary>

```python
class ScriptSequence(AbstractScriptSequence):
    """
    This class defines a simple scripted Sequence running pings between two Nodes.

    It is meant to be used together with the simple-switch_topology.yml topology.
    """

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
        """
        Define the sequential / script simulation sequence to be run.

        Add commands like you would in the rettij CLI:

        .. code-block:: python

            nodes.n1.ping(nodes.n2.ifaces.i0.ip)

        All parameters are automatically supplied upon execution by rettij.

        :param sm: SimulationManager controlling the simulation.
        :param node: NodeContainer object with all simulation Nodes (alternative to 'nodes', same object referenced).
        :param nodes: NodeContainer object with all simulation Nodes (alternative to 'node', same object referenced).
        :param channel: Map of all simulation Channels (alternative to 'channels', same object referenced).
        :param channels: Map of all simulation Channels (alternative to 'channel', same object referenced).
        """
        nodes.client0.ping(target=nodes.client1.ifaces.i0.ip.compressed, c=1)
        nodes.client1.ping(target=nodes.client0.ifaces.i0.ip.compressed, c=1)
```

</details>

This is the signature for a `ScheduledSequence`:
```python
class ScheduledSequence(AbstractScheduledSequence):

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
```

<details>
<summary>CLICK HERE to show a `ScheduledSequence` example</summary>

```python
class ScheduledSequence(AbstractScheduledSequence):
    """
    This class defines a simple scheduled Sequence running pings between two Nodes.

    It is meant to be used together with the simple-switch_topology.yml topology.
    """

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
        """
        Define the timed steps simulation sequence to be run.

        Add timed steps like this:

        .. code-block:: python

            sm.add_step(
                scheduled_time=1,
                command = nodes.client0.ping,
                args = (nodes.client1.ifaces.i0.ip,),
                kwargs = {'c': 10}
            )

        All parameters are automatically supplied upon execution by rettij.

        :param sm: SimulationManager controlling the simulation.
        :param node: NodeContainer object with all simulation Nodes (alternative to 'nodes', same object referenced).
        :param nodes: NodeContainer object with all simulation Nodes (alternative to 'node', same object referenced).
        :param channel: Map of all simulation Channels (alternative to 'channels', same object referenced).
        :param channels: Map of all simulation Channels (alternative to 'channel', same object referenced).
        """
        sm.add_step(
            scheduled_time=10, command=nodes.client0.ping, kwargs={"target": nodes.client1.ifaces.i0.ip, "c": 1}
        )
        sm.add_step(
            scheduled_time=20, command=nodes.client1.ping, kwargs={"target": nodes.client0.ifaces.i0.ip, "c": 1}
        )
```

</details>

A sequence is NOT required to run rettij.
By default, no sequence is used.

Example sequences are located at `rettij/examples/sequences`:

* `script_ping_sequence.py`: Simply executes two ping-commands (_ScriptSequence_).
* `scheduled_ping_sequence.py`: Simply executes two ping-commands (_ScheduledSequence_).
* `tcp_dump_sequence.py`: Runs ping commands, a tcpdump and prints the outputs of the commands (_ScheduledSequence_).

## Tell rettij how to access the Kubernetes cluster
Since rettij needs to access the Kubernetes cluster, you need to provide the information necessary to facilitate that access.
* The `kubeconfig` file contains information needed by the Kubernetes client in order to connect to the cluster.
* The path to the configuration can be passed to the `main.py` script by using the parameter `--config <path-to-file>`.
A more in-depth explanation can be found below in section "[Cluster access via configuration file](#cluster-access-via-configuration-file)".
* You can also supply the information necessary to access the cluster by using environment variables, see section "[Cluster access via environment variables](#cluster-access-via-environment-variables)".

### Cluster access via configuration file
By default, Rettij will look for a kubeconfig file in `~./.kube/config`.
This file is generated automatically by Kubernetes and contains the kubectl configuration necessary to access the cluster.
You can also use your own configuration file path.
See ["Command line parameters"](#command-line-parameters).

<details>
<summary>CLICK HERE to show an example kubeconfig file</summary>

```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: <omitted>
    server: https://localhost:6443
  name: default
contexts:
- context:
    cluster: default
    user: default
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: default
  user:
    password: <omitted>
    username: admin
```

</details>

### Cluster access via environment variables
Rettij can also access a cluster if the following environment variables are set properly:
*  `KUBE_TOKEN`: API access token for Kubernetes
*  `KUBE_URL`: URL of a cluster master node (i.e. `https://<cluster master node ip or hostname>:6443`)
*  `KUBE_CA_PEM`: Full path to a file containing the cluster certificate authority data (i.e. `<rettij_root_dir>/kube_ca.pem`)

Instructions on how to extract this information from the cluster can be found [here](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#existing-kubernetes-cluster).


## Integrate rettij

There are currently two ways to integrate rettij into another environment.
Integration into any Python-based software can be done by using the main `Rettij` class.
Integration for co-simulations using `mosaik` is done via the `RettijMosaik` class.

### Direct integration (Python script)
You can easily integrate rettij into your own scripts by importing the main `Rettij` class from `rettij.py`.

This class offers the following methods to control / run the simulation:

| Method       | Required  | Callback | Requirements            | Purpose                                         |
|--------------|-----------|----------|-------------------------|-------------------------------------------------|
| `init()`     | Yes       | Yes      | None                    | Load and parse the configuration files          |
| `create()`   | Yes       | Yes      | `init()`                | Start the simulation environment                |
| `step()`     | No        | Yes      | `init()` and `create()` | Run a SINGLE simulation step                    |
| `finalize()` | Yes       | Yes      | None                    | Clean up the simulation environment             |

**Important note:** When errors are encountered, rettij will automatically `finalize()` the simulation.
This is done make sure that the simulation environment is always cleaned up if possible.

In order to check if there are more steps to be run, use `rettij.has_next_step(current_time)`.

All of the methods listed in the table above also accept a callback function that will be executed when the method has finished.

You may refer to the code in the `main.py` to see how to integrate rettij into a script.

### Co-simulation (Mosaik Scenario)
In order to integrate rettij into a mosaik co-simulation scenario, you should to use the `RettijMosaik` class.
This class handles mosaik's meta model and passes data to and from the main `Rettij` class.

Using rettij (or any simulator for that matter) inside a mosaik scenario entails the following basic steps:
1. Define simulator parameters
1. Start simulator
1. Start the simulator model(s)
1. Connect entities for data exchange

Please refer to the mosaik documentation for more information and examples.

#### 1. Define Simulator Parameters
Simulator parameters have to be passed as a dictionary of type `Dict[str, Any]`.
`RettijMosaik` uses the following parameter keys:

| Key                   	| Type                                       	| Purpose                                                        	|
|-----------------------	|--------------------------------------------	|----------------------------------------------------------------	|
| `sim_name`               	| `str`                          	            | (Required) Simulator name used by `mosaik`                        |
| `topology_path`       	| `str` or `Path()`                          	| (Required) Path to topology file                                  |
| `sequence_path`       	| `str` or `Path()`                          	| (Optional) Path to simulation sequence file                    	|
| `kubeconfig_path`     	| `str` or `Path()`                          	| (Optional) Path to kubeconfig file                             	|
| `components_dir_path` 	| `str` or `Path()`                          	| (Optional) Path to the custom components root directory        	|
| `step_size`           	| `int`                                      	| (Optional) Simulation time increment ("step size"), default: 1 	|
| `file_loglevel`       	| `rettij.common.logging_utilities.Loglevel` 	| (Optional) Set loglevel for logfile, default: INFO             	|
| `console_loglevel`    	| `rettij.common.logging_utilities.Loglevel` 	| (Optional) Set loglevel for console, default: WARNING          	|

Example:
```python
rettij_params = {
    "sim_name": "Rettij",
    "topology_path": Path(__file__) / "topology.yml",
    "kubeconfig_path": "",
    "sequence_path": "",
    "components_dir_path": Path(__file__) / "custom_components",
    "step_size": 50,
    "file_loglevel": Loglevel.DEBUG,
    "console_loglevel": Loglevel.INFO,
}
```

Note: The `sim_name` key is not used by rettij.
It is required by mosaik for naming the Simulators.

#### 2. Start Simulator
Each simulator needs to be started using mosaik:
```
rettij_sim = world.start(**rettij_params)
```

The `RettijMosaik` meta model object can then be accessed via `rettij_sim`.

#### 3. Start the Simulator Model(s)
By design, each simulator in mosaik can have multiple models, i.e. instances with different parameters.
**Limitation:** Since rettij is using a live system in the background, only a single instance can be used.

However, each Node in the topology can be represented as a distinct entity by using the following:

```python
model_rettij_sim = sim_rettij.Rettij()  # Create the main Rettij model. Will instantiate RettijMosaik.
rettij_sim_nodes = {  # Store a model for each node.
    node_entity.eid: node_entity
    for node_entity in model_rettij_sim.children
}
```

The `RettijMosaik` instance can then be accessed via `model_rettij_sim`.
The specific Nodes can be accessed via `rettij_sim_nodes["<node name>"]`.

#### 4. Connect Entities for Data Exchange
In order to exchange data through mosaik, entities must be connected to one another.
Please refer to the mosaik documentation for more information.
Additionally, communication targets within rettij must be set via `MosaikRettij.connect()` (see [Hooks](#hooks)).

```python
world.connect(
    some_sim_model_0,
    rettij_sim_nodes["some-sim-model-0"],
    "some_attribute"
)
world.connect(
    rettij_sim_nodes["some-sim-model-1"],
    some_sim_model_1,
    "some_attribute"
)
rettij_sim.connect(
    rettij_sim_nodes["some-sim-model-0"],
    rettij_sim_nodes["some-sim-model-1"],
    attr="some_attribute"
)
```

## Useful Kubernetes Commands
Delete all rettij namespaces including their pods. This is useful in case a rettij simulation has crashed without finalizing, in which case the namespace it was run in will persist until removed manually.
```bash
## With sudo
sudo kubectl delete namespace $(sudo kubectl get namespace -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}" | grep 'rettij' )

## Without sudo
kubectl delete namespace $(kubectl get namespace -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}" | grep 'rettij' )

## Powershell
kubectl delete namespace $(kubectl get namespace -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}" | Select-String -Pattern 'rettij')

List all nodes, namespaces, pods with extra info:
```bash
kubectl get nodes -o wide
kubectl get namespaces -o wide
kubectl get --all pods -o wide
```

## Kubernetes and rettij

This section should give you a short overview about the collaboration between rettij and Kubernetes.

### Overview Kubernetes

The image below shows an exemplary Kubernetes Cluster.
![Overview Kubernetes](diagrams/overview-kubernetes/overview-kubernetes.png "Overview Kubernetes")

Please refer [What is Kubernetes?](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) for a better understanding about Kubernetes.

### Interaction between Kubernetes and rettij
The following diagram presents the interaction between Kubernetes and rettij.
![Interaction between Kubernetes and rettij](diagrams/interaction-rettij-kubernetes/interaction-rettij-kubernetes.png "Interaction between Kubernetes and rettij")

### Topology and Simulation Objects
The built topology, which is based on YAML files, is used to instantiate simulation objects.

_TODO_: Update the diagram.

![Topology and Simulation Objects](diagrams/topology-and-simulation-objects/topology-and-simulation-objects.png "Topology and Simulation Objects")

### Simulation Setup
A simplified call sequence for setting up the simulation is shown below.

![Simulation Setup (Simplified)](diagrams/simulation-setup-kubernetes-simplified/simulation-setup-kubernetes-simplified.png "Simulation Setup (Simplified)")

### Connect Hook
The call sequence for the `connect` hook is shown below.

![Connect Hook](diagrams/connect-hook/connect-hook.png "Connect Hook")
