#!/bin/bash

# Purpose: Build bridge image and push it to a registry
# Usage: build_simpleswitch.sh <repository>
# Example: build_simplerouter.sh "frihsb/rettij"

function build {
  repository=$1

  if [ -f "Dockerfile" ]
  then
    export DOCKER_BUILDKIT=1
    version=$(cat VERSION)
    docker buildx create --use
    docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --push --tag "$repository"_simple-switch:"$version" --tag "$repository"_simple-switch:latest .
    docker buildx rm -f
    docker buildx prune -f
  fi
}

cd "$(dirname "$0")" || exit

if [ "${#}" -eq 1 ]; then
  build "$1"
elif [ "${#}" -eq 0 ]; then
  echo "Pushing to frihsb/rettij"
  build "frihsb/rettij"
elif [ "${#}" -gt 1 ]; then
  echo "Error: Too many arguments!"
  echo "Usage: build_simpleswitch.sh <repository>"
fi
